import { getCookie } from '@/utilities/cookie'
import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '@/views/visitor/LoginView.vue'
import DashboardView from '@/views/authenticated/DashboardView.vue'
import LeaveView from '@/views/authenticated/LeaveView.vue'
import AttendanceView from '@/views/authenticated/AttendanceView.vue'
import UserView from '@/views/authenticated/UserView.vue'
import NotFoundView from '@/views/errors/NotFoundView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/:pathMatch(.*)',
      name: 'NotFound',
      component: NotFoundView,
    },
    {
      path: '/',
      component: LoginView,
      beforeEnter: (_to, _from, _next) => {
        getCookie('token') ? _next({ name: 'Dashboard' }) : _next()
      }
    },  
    {
      path: '/login',
      name: 'Login',
      component: LoginView,
      beforeEnter: (_to, _from, _next) => {
        getCookie('token') ? _next({ name: 'Dashboard' }) : _next()
      }
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: DashboardView,
      beforeEnter: (_to, _from, _next) => {
        getCookie('token') ? _next() : _next({ name: 'Login' })
      }
    },
    {
      path: '/attendance',
      name: 'Attendance',
      component: AttendanceView,
      beforeEnter: (_to, _from, _next) => {
        getCookie('token') ? _next() : _next({ name: 'Login' })
      }
    },
    {
      path: '/leaves',
      name: 'Leaves',
      component: LeaveView,
      beforeEnter: (_to, _from, _next) => {
        getCookie('token') ? _next() : _next({ name: 'Login' })
      }
    },
    {
      path: '/users',
      name: 'Users',
      component: UserView,
      beforeEnter: (_to, _from, _next) => {
        getCookie('token') ? _next() : _next({ name: 'Login' })
      }
    },
  ]
})

export default router
