import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

//Quasar Framework
import { Quasar, Dialog, Notify, Loading, LoadingBar } from 'quasar'
import '@quasar/extras/material-icons/material-icons.css'
import 'quasar/src/css/index.sass'
import '@/assets/styles/main.scss'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(Quasar, {
    plugins: {
        Notify,
        Dialog,
        Loading,
        // LoadingBar
    },
    // config: {
    //     loadingBar: { 
    //         color: 'primary',
    //         size: '4px',
    //         position: 'top'
    //     }
    // }
})

app.mount('#app')
