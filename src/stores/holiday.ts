/* eslint-disable no-useless-catch */
import { defineStore } from 'pinia'
import { axiosInstance } from '@/utilities/axios'
import { API_URL } from '@/utilities/constants'
import type { HolidayResponse } from '@/utilities/interfaces/holiday';

export const useHolidayStore = defineStore('holiday', {
  state: () => ({
    holidays: [] as Array<HolidayResponse>,
  }),
  getters: {
    hasHolidays: (state) => state.holidays != null,
  },
  actions: {
    async getHolidays () : Promise<Array<HolidayResponse>> {
			try {
				const response = await axiosInstance.get(`${API_URL}/holidays`),

				data = response.data;
				return this.holidays = data;
			} catch (error) {
				throw error
			}
    },
  },
});
