/* eslint-disable no-useless-catch */
import { defineStore } from 'pinia'
import { axiosInstance } from '@/utilities/axios'
import { API_URL } from '@/utilities/constants'
import type { LeaveResponse } from '@/utilities/interfaces/leave';

export const useUserLeave = defineStore('user-leave', {
  state: () => ({
    leaves: [] as Array<LeaveResponse>,
		pagination: {}
  }),
  getters: {
    hasLeaves: (state) => state.leaves != null,
  },
  actions: {
    async getLeaves(
      branchId: number | null,
      departmentId: number | null,
      startDate: string,
      endDate: string
    ): Promise<LeaveResponse[]> {
      try {
        const response: any = await axiosInstance.get(`${API_URL}/user/leaves`, {
          params: {
            'branch_id': branchId,
            'department_id': departmentId,
            'start_date': startDate,
            'end_date': endDate
          }
        });

        const data = response.data
				const pagination = response.pagination

        this.leaves = data
				this.pagination = pagination

        return data;
      } catch (error) {
        throw error;
      }
    },

    async createLeave(leaveInfo: Partial<LeaveResponse>): Promise<LeaveResponse> {
      try {
        const response = await axiosInstance.post(
          `${API_URL}/user/leaves`,
          leaveInfo
        );

        const data = response.data as LeaveResponse;
        
        return data;
      } catch (error) {
        throw error;
      }
    },

    async updateLeave(leaveId: Number, leaveInfo: Partial<LeaveResponse>): Promise<LeaveResponse> {
      try {
        const response = await axiosInstance.patch(
          `${API_URL}/user/leaves/${leaveId}`,
          leaveInfo
        );

        const data = response.data as LeaveResponse;
        
        return data;
      } catch (error) {
        throw error;
      }
    },

    async deleteLeave(leaveId: Number): Promise<boolean> {
      try {
        await axiosInstance.delete(
          `${API_URL}/user/leaves/${leaveId}`
        )

        return true;
      } catch (error) {
        throw error
      }
    }
  },
});
