/* eslint-disable no-useless-catch */
import { defineStore } from 'pinia'
import { axiosInstance } from '@/utilities/axios'
import { API_URL } from '@/utilities/constants'
import { getCookie } from '@/utilities/cookie'

export const useUserAttendance = defineStore('user-attendance', {
  state: () => ({
    attendance: [] as Array<Record<string, any>>,
    currentTimeInId: null as number | null
  }),
  getters: {
    hasAttendance: (state) => state.attendance != null,
  },
  actions: {
    async getAttendance (
      branchId: number | null,
      departmentId: number | null,
      startDate: string | null = null,
      endDate: string | null = null
    ) : Promise<Array<Record<string, number>>> {
        try {
          const response = await axiosInstance.get(`${API_URL}/user/attendance`, {
            params: {
              'branch_id': branchId,
              'department_id': departmentId,
              'start_date': startDate,
              'end_date': endDate
            }
          }),

        	data = response.data;
          return this.attendance = data;
        } catch (error) {
          throw error
        }
    },
    async timeIn() : Promise<any>{
      try {
        await axiosInstance.post(`${API_URL}/user/attendance/time-in`, {
          'time_in': true
        }, {
          headers: {
            'Authorization': `Bearer ${getCookie('token')}`
          }
        })
      } catch (error) {
        throw error
      }
    },
    async timeOut() : Promise<any>{
      try {
        await axiosInstance.post(`${API_URL}/user/attendance/time-out`, {
          'time_out': true
        }, {
          headers: {
            'Authorization': `Bearer ${getCookie('token')}`
          }
        })
        
      } catch (error) {
        throw error
      }
    },
  }
});
