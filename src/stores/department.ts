/* eslint-disable no-useless-catch */
import { defineStore } from 'pinia'
import { axiosInstance } from '@/utilities/axios'
import { API_URL } from '@/utilities/constants'

export const useDepartmentStore = defineStore('department', {
  state: () => ({
    departments: [] as Array<Record<string, any>>,
  }),
  getters: {
    hasDepartments: (state) => state.departments != null,
  },
  actions: {
    async getDepartments () : Promise<Array<Record<string, number>>> {
			try {
				const response = await axiosInstance.get(`${API_URL}/departments`),

				data = response.data;
				return this.departments = data;
			} catch (error) {
				throw error
			}
    },
  },
});
