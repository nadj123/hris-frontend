/* eslint-disable no-useless-catch */
import { defineStore } from 'pinia';
import { axiosInstance } from '@/utilities/axios';
import { API_URL } from '@/utilities/constants';
import { deleteAllCookies, setCookie } from '@/utilities/cookie';
import { useAccountStore } from './account';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    token: '' as string,
  }),
  getters: {
    hasToken: (state) => state.token != null,
  },
  actions: {
    async login (
      email: string,
      password: string, 
    ) : Promise<string> {
      try {
        const response = await axiosInstance.post(`${API_URL}/auth/login`, {
          email: email,
          password: password
        })

        const data = response.data;
        setCookie('token', data.token);

        return this.token = data.token;
      } catch (error) {
        throw error
      }
    },

    async logout() : Promise<any>{
      try {
        await axiosInstance.post(`${API_URL}/auth/logout`, {})
        const accountStore = useAccountStore()

        deleteAllCookies()
        accountStore.account = null

        return this.token = ''
      } catch (error) {
        throw error
      }
    },
  },
});
