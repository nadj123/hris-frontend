/* eslint-disable no-useless-catch */
import { defineStore } from 'pinia'
import { axiosInstance } from '@/utilities/axios'
import { API_URL } from '@/utilities/constants'
import { getCookie, setCookie } from '@/utilities/cookie'

export const useAccountStore = defineStore('account', {
  state: () => ({
    account: {} as Record<string, any> | null,
  }),
  getters: {
    hasAccountInformation: (state) => state.account != null,
  },
  actions: {
    async getAccountInfo () : Promise<Response> {
        try {
          if (getCookie('account')) {
            return this.account = JSON.parse(getCookie('account') ?? '')
          }

          const response = await axiosInstance.get(`${API_URL}/auth/me`),
          data = response.data;

          setCookie('account', JSON.stringify(data))
          return this.account = data;

        } catch (error) {
          throw error
        }
    },
  },
});
