/* eslint-disable no-useless-catch */
import { defineStore } from 'pinia'
import { axiosInstance } from '@/utilities/axios'
import { API_URL } from '@/utilities/constants'

export const usepositionStore = defineStore('position', {
  state: () => ({
    positions: [] as Array<Record<string, any>>,
  }),
  getters: {
    hasPositions: (state) => state.positions != null,
  },
  actions: {
    async getPositions () : Promise<Array<Record<string, number>>> {
		try {
			const response = await axiosInstance.get(`${API_URL}/positions`),

			data = response.data;
			return this.positions = data;
		} catch (error) {
			throw error
		}
    },
  },
});
