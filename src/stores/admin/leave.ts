/* eslint-disable no-useless-catch */
import { defineStore } from 'pinia'
import { axiosInstance } from '@/utilities/axios'
import { API_URL } from '@/utilities/constants'
import { getCookie } from '@/utilities/cookie'
import type { LeaveResponse } from '@/utilities/interfaces/leave';

export const useAdminLeave = defineStore('admin-leave', {
  state: () => ({
    leave: {} as LeaveResponse,
  }),
  getters: {
    hasLeave: (state) => state.leave != null,
  },
  actions: {
    async approveLeave(
      leaveId: number,
    ): Promise<LeaveResponse> {
      try {
        const response: any = await axiosInstance.patch(`${API_URL}/admin/leave/approve/${leaveId}`, {}, {
          headers: {
            'Authorization': `Bearer ${getCookie('token')}`
          }
        });

        const data = response.data
        return this.leave = data
      } catch (error) {
        throw error;
      }
    },
    async declineLeave(
      leaveId: number,
    ): Promise<LeaveResponse> {
      try {
        const response: any = await axiosInstance.patch(`${API_URL}/admin/leave/decline/${leaveId}`, {}, {
          headers: {
            'Authorization': `Bearer ${getCookie('token')}`
          }
        });

        const data = response.data
        return this.leave = data
      } catch (error) {
        throw error;
      }
    }
  },
});
