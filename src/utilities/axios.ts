import axios, { type AxiosInstance, type AxiosResponse } from 'axios';
import { API_BASE_URL } from './constants';
import { NETWORK_ERROR, UNAUTHORIZED } from './error';
import { deleteAllCookies, getCookie } from './cookie';
import { useAccountStore } from '@/stores/account';
import router from '@/router'
import { setupCache } from 'axios-cache-adapter';

const cache = setupCache({
  maxAge: 15 * 60 * 1000, // Cache responses for 15 minutes
});

const axiosInstance: AxiosInstance = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  adapter: cache.adapter
});

axiosInstance.interceptors.request.use(config => {
  const token = getCookie('token');
  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`;
  }
  return config;
});

axiosInstance.interceptors.response.use(
  (response: AxiosResponse) => response.data,
  (error) => {
    const account = useAccountStore().account;
    if (account || getCookie('token')) {
      const isUnauthorized = error.response?.statusText === UNAUTHORIZED;
      const isNetworkError = error.message === NETWORK_ERROR;
      if (isUnauthorized || isNetworkError) {
        router.push('/login');
        useAccountStore().account = null;
        deleteAllCookies();
      }
    }
    return Promise.reject(error);
  }
);

export { axiosInstance };
