export function successMessageOpts(message: string): Record<string, any> {
  return {
    position: 'top',
    color: 'green',
    textColor: 'white',
    icon: 'thumbs_up',
    message: message
  }
}

export function errorMessageOpts(message: string): Record<string, any> {
  return {
    position: 'top',
    color: 'red',
    textColor: 'white',
    icon: 'error',
    message: message
  }
}
