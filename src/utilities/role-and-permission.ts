// Define an interface for the Role object
interface Role {
  id: number;
  name: string;
  guard_name: string;
  created_at: string;
  updated_at: string;
  permissions: Permission[];
}

// Define an interface for the Permission object
interface Permission {
  id: number;
  name: string;
  guard_name: string;
  created_at: string;
  updated_at: string;
  pivot: {
    role_id: number;
    permission_id: number;
  };
}

// ...

// Check if "approver" exists in the permissions array for each role
export const hasApproverPermission = (roles: Role[] = []): boolean => {
  return roles.some((role: Role) => {
    return Array.isArray(role.permissions) && role.permissions.some((permission: Permission) => {
      return permission.name === "approver";
    });
  });
};

// ...
