export interface Branches {
	id: number,
	name: string,
}

export interface BranchesResponse {
	id: number,
	name: string,
}
