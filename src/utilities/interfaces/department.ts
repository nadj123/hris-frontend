export interface Department {
	id: number,
	name: string,
	hex_code: string,
}

export interface DepartmentResponse {
	id: number,
	name: string,
	hexCode: string
}
