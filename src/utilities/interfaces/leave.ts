import { type User, type UserResponse } from './user'

export interface Leave {
	id: number,
	approvedBy: Record<string, any> | null,
	type: number,
	halfDay: number,
	postMeridiem: boolean,
	startDate: string,
	endDate: string,
	reason: string,
	initialApprover: number,
	status: number,
	user: User
}

export interface LeaveResponse {
	id: number,
	approved_by: Record<string, any> | null,
	type: number,
	half_day: number,
	post_meridiem: number,
	start_date: string,
	end_date: string,
	reason: string,
	initial_approver: number,
	status: number,
	user: UserResponse
}