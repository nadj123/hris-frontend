export interface Holiday {
	id: number,
	country: string,
	title: string,
	description: string,
	startDate: string,
	endDate: string,
}

export interface HolidayResponse {
	id: number,
	country: string,
	title: string,
    description: string,
	start_date: string,
	end_date: string,
}
