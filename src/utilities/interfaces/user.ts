export interface User {
	id: number,
	email: string,
    emailVerifiedAt: string,
    temporaryName: string,
    information: Record<string, any>,
    createdAt: string,
    updatedAt: string
}

export interface UserResponse {
	id: number,
	email: string,
    email_verified_at: string,
    temporary_name: string,
    information: Record<string, any>,
    created_at: string,
    updated_at: string
}