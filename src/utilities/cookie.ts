import Cookies from 'js-cookie';

export function setCookie(name: string, value: string, days: number = 15): void {
  const currentDate = new Date();
  const expiresDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0);
  expiresDate.setDate(expiresDate.getDate() + days);

  Cookies.set(name, value, { expires: expiresDate });
}

export function getCookie(name: string): string | undefined {
  return Cookies.get(name);
}

export function deleteCookie(name: string): void {
  Cookies.remove(name);
}

export function deleteAllCookies(): void {
  const cookieNames = Object.keys(Cookies.get());

  cookieNames.forEach((cookieName) => {
    Cookies.remove(cookieName);
  });
}
