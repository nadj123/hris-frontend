export const VALIDATION_FAILED = 'validation_failed';
export const INVALID_LOGIN = 'invalid_login';
export const UNAUTHORIZED = 'Unauthorized';
export const NETWORK_ERROR = 'Network Error';

export function catchValidationMessages(errorData: any): Record<string, any> {
  let errors: Record<string, any> = {};
  const errorCode: string = errorData.error.code;
  const newErrors: Record<string, any> = errorData.error.validation_errors;

  if (newErrors && errorCode === VALIDATION_FAILED) {
    errors = newErrors;
  }

  return errors;
}