import moment from "moment";

export const API_BASE_URL: string = 'http://localhost:8099';
export const API_URL: string = `${API_BASE_URL}/api/v1`;

export const leaveStatuses = [
    {
        id: 1,
        name: 'For Review',
        hex_code: '#34ace0'
    },
    {
        id: 2,
        name: 'Approved',
        hex_code: '#C4E538'
    },
    {
        id: 3,
        name: 'Declined',
        hex_code: '#EA2027'
    }
];

export const holidayCountries = [
    {
        id: 1,
        name: 'Japan',
        hex_code: '#7CB342'
    },
    {
        id: 2,
        name: 'Philippines',
        hex_code: '#7CB342'
    }
];

export const attendanceStatuses = [
    {
        id: 1,
        name: 'Early',
        hex_code: '#31CCEC'
    },
    {
        id: 2,
        name: 'Late',
        hex_code: '#F2C037'
    },
    {
        id: 3,
        name: 'Early Out',
        hex_code: '#EA2027'
    },
];

export const attendanceLocations = [
    {
        id: 1,
        name: 'Office',
        hex_code: '#4a69bd'
    },
    {
        id: 1,
        name: 'Work from home',
        hex_code: '#1abc9c'
    }
]

export function updateOrPushArrayById(
    dataArray: any, 
    newItem: any
): void {
    const index = dataArray.findIndex((item: any) => item.id === newItem.id);
    if (index !== -1) {
        dataArray[index] = newItem;
    } else {
        dataArray.push(newItem);
    }
}

export function deleteArrayDataById(
    dataArray: any, 
    id: Number
): void {
    const index = dataArray.findIndex((item: any) => item.id === id)

    if (index !== -1) {
        delete dataArray[index]
    }
}

export function getBusinessDays (startDate: string, endDate: string) {
    const lastDay = moment(endDate);
    const firstDay = moment(startDate);
    let calcBusinessDays = 1 + (lastDay.diff(firstDay, 'days') * 5 -
        (firstDay.day() - lastDay.day()) * 2) / 7;

    if (lastDay.day() == 6) calcBusinessDays--;//SAT
    if (firstDay.day() == 0) calcBusinessDays--;//SUN

    return calcBusinessDays;
}