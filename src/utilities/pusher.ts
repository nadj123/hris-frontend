import { ref, onMounted, onUnmounted } from 'vue';
import pusher from '@/plugins/pusher';

export default function usePusher(channelName: string, eventName: string) {
  const channel = pusher.subscribe(channelName);
  const eventData = ref<any>(null);

  const handleEvent = (data: any) => {
    // Update the eventData with the received data
    eventData.value = data;
    // Perform any additional actions with the data
  };

  const unsubscribe = () => {
    pusher.unsubscribe(channelName);
  };

  onMounted(() => {
    channel.bind(eventName, handleEvent);
  });

  onUnmounted(() => {
    channel.unbind(eventName, handleEvent);
    unsubscribe();
  });

  return {
    eventData,
  };
}
